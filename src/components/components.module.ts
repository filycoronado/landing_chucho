import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwlModule } from 'ngx-owl-carousel';  

// Components
import { SliderComponent } from './slider/slider.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from '../app/app-routing.module';

@NgModule({
    imports: [
        CommonModule,
        OwlModule,
        AppRoutingModule
    ],
    exports: [
        SliderComponent,
        NavbarComponent,
        FooterComponent
    ],
    declarations: [
        SliderComponent,
        NavbarComponent,
        FooterComponent
    ],
    providers: [],
 })
 
 export class ComponentsModule {
 }